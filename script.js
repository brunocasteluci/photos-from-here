let latitude = -25.4073574,
  longitude = -49.2941253;
let imageNumber = 0;
let photoDiv = document.getElementById('photo-div');
let nextButton = document.createElement('button');

function userLocation() {
  function success(position) {
    latitude = position.coords.latitude;
    longitude = position.coords.longitude;
  }

  function error() {
    console.log('Unable to retrieve your location');
    latitude = -25.4073574;
    longitude = -49.2941253;
  }

  if (!navigator.geolocation) {
    latitude = position.coords.latitude;
    longitude = position.coords.longitude;
  } else {
    navigator.geolocation.getCurrentPosition(success, error);
  }
}

userLocation();

function urlFlickr() {
  userLocation();
  return `https://www.flickr.com/services/rest/?api_key=ccee99e323f5a9a39373cd55f8199c55&format=json&nojsoncallback=1&method=flickr.photos.search&safe_search=1&per_page=5&lat=${latitude}&lon=${longitude}&text=cachorros`;
}

function fetchFlickr() {
  let url = urlFlickr();
  fetch(url)
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      let imageUrl = constructImageURL(data.photos.photo[imageNumber]);
      showPhotos(imageUrl);
      nextImage();
      nextButton.addEventListener('click', () => {
        imageNumber++;
        imageUrl = constructImageURL(data.photos.photo[imageNumber]);
        showPhotos(imageUrl);
        if (imageNumber === 4) {
          imageNumber = -1;
        }
      });
    });
}

function constructImageURL(photoObj) {
  return (
    'https://farm' +
    photoObj.farm +
    '.staticflickr.com/' +
    photoObj.server +
    '/' +
    photoObj.id +
    '_' +
    photoObj.secret +
    '.jpg'
  );
}

function showPhotos(imageUrl) {
  let showPhoto = document.createElement('img');
  showPhoto.src = imageUrl;
  showPhoto.style.height = '150px';
  showPhoto.style.width = '150px';
  showPhoto.style.margin = '5px';
  photoDiv.appendChild(showPhoto);
}

function nextImage() {
  nextButton.innerHTML = 'Proximo';
  photoDiv.appendChild(nextButton);
}

fetchFlickr();
